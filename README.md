# Fullstack challenge

## Objective

The objective is to build a web-application that does some analysis of a web-page/URL. The application should show a form with a text-field thus the user can type in the URL of the webpage being analyzed. Additionally to form should contain a button for sending the input to the server as well as to trigger the server-side analysis process. After processing the results should be shown to the user below the form, design and user experience matters, but it's your decision how to represent the results. 

The result comprises the following information:
 - What HTML version has the document?
 - What is the page title?
 - How many headings of what level are in the document?
 - How many internal and external links are in the document? 
 - Are there any inaccessible links and how many?
 - Did the page contain a login-form? 

In case the URL given by the user is not reachable an error message should appear below the input form. The message should contain the HTTP status-code and some useful error description. 

## Requirements

The backend has to be set up as SBT project and built using Scala. Frontend should be a React app.

*HINT*: for document analysis consider using a library such as Jsoup! 

The result should be:

-  Provided as a public git repository (ideally with the commit history). -  A short README  document that lists the main steps of building your solution as well as all assumptions/decisions you made in case of unclear requirements or missing information. 

## What we look at

- The project structure backend/frontend
- Frontend <> Backend interaction
- Frontend applications UI/UX
- Coding style, best practices and tests

Thank you for your cooperation!
